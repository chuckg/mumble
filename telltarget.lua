mumbleTellTarget = {}

local string_format = format

local function tellTarget(msg)
   if not (UnitExists("target")
	  and UnitName("target")
	  and UnitIsPlayer("target")
	  and GetDefaultLanguage("player") == GetDefaultLanguage("target"))
   or not (msg and strlen(msg)>0) then
	  return
   end
   
   local name, realm = UnitName("target")
   if realm and realm ~= GetRealmName() then
	  name = string_format("%s-%s", name, realm)
   end
   
   SendChatMessage(msg, "WHISPER", nil, name)
end

function mumbleTellTarget:Enable()
   SlashCmdList["TELLTARGET"] = function(str) tellTarget(str) end
   SLASH_TELLTARGET1 = "/tt"
end