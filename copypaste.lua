mumbleCopyPaste = {}

local _G = getfenv(0)
local string_match = strmatch
local string_sub = strsub
local string_gsub = gsub
local math_fmod = math.fmod

local lastMessageID = {0, 0, 0, 0, 0, 0, 0}
local buffer = {}
local bufferSize = 128

local function strip(str)
	str = string_gsub(str, "|Hplayer:.-%[(.-)%]|h", "%1")
	str = string_gsub(str, "(|c%x%x%x%x%x%x%x%x(.-)|r)", function (full, text) 
		if not string_match(text, "^|H.-|h$") then 
			return text 
		end
	end)
	return str
end

function mumbleCopyPaste:Enable(symbol, color, disable)
	if type(disable) ~= "string" then disable = nil end

	-- Hook :AddMessage() and preprend our custom ItemRef.
	for i=1,7 do
		if not disable or disable and not string_match(disable, i) then
			local f = _G["ChatFrame"..i]
			local add = f.AddMessage
			f.AddMessage = function(this,text,r,g,b,id)
				local chatFrameID = tonumber(string_sub(this:GetName(), 10))
				local messageID = lastMessageID[chatFrameID] + 1
				lastMessageID[chatFrameID] = messageID
				
				buffer[math_fmod(messageID - 1, bufferSize) + (1 + ((chatFrameID - 1) * bufferSize))] = text
				text = "|cff".. color .."|Hmumble:".. chatFrameID ..":".. messageID .."|h".. symbol .."|h|r ".. text

				add(this,text,r,g,b,id)
			end
		end
	end
	
	-- Hook SetItemRef
	local ref = SetItemRef
	SetItemRef = function(link, text, button)
		if (string_sub(link, 1, 7) ~= "mumble:") then
			return ref(link, text, button)
		end
		local chatFrameID, messageID = string_match(link, "^mumble:(%d+):(%d+)$")
		local text = strip(buffer[math_fmod(messageID - 1, bufferSize) + (1 + ((chatFrameID - 1) * bufferSize))])

		local editbox = _G["ChatFrameEditBox"]
		if text and editbox then
			editbox:SetText(text)
			editbox:Show()
			editbox:SetFocus()
		end
	end
end