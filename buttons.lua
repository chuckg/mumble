mumbleButtons = {}

local _G = getfenv(0)

function mumbleButtons:Enable(showUp, showDown, showBottom)
	local b
	ChatFrameMenuButton:Hide()
	for i = 1,7 do
		if not showUp then
			b = _G["ChatFrame"..i.."UpButton"]
			b:SetScript("OnShow", function() this:Hide() end)
			b:Hide()
		end

		if not showDown then
			b = _G["ChatFrame"..i.."DownButton"]
			b:SetScript("OnShow", function() this:Hide() end)
			b:Hide()
		end

		if not showBottom then
			b = _G["ChatFrame"..i.."BottomButton"]
			b:SetScript("OnShow", function() this:Hide() end)
			b:Hide()
		end
	end
end