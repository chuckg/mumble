mumbleTimeStamps = {}

local _G = getfenv(0)
local string_format = format
local string_sub = strsub

function mumbleTimeStamps:Enable(format, milli, color, outputFormat, disable)
	if type(disable) ~= "string" then disable = nil end
	
	-- Here's my hokey ass version of millisecond precision, since it's damn near impossible to get a "true" version anyway.
	getMillisecond = function()
		if milli then
			return string_sub(string_format("%02.3f", GetTime()), -4)
		end
		return ""
	end
	
	for i=1,7 do
		if not disable or disable and not strmatch(disable, i) then
			local f = _G["ChatFrame"..i]
			local add = f.AddMessage
			
			f.AddMessage = function(this,text,r,g,b,id)
				if type(text) == "string" then
					text = string_format("|cff"..color..outputFormat, date(format)..getMillisecond(), text or "")
				end
				add(this,text,r,g,b,id)
			end
		end
	end
end