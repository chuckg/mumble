mumbleEditBox = {}

local _G = getfenv(0)

function mumbleEditBox:Enable(arrowKeys, top)
	if arrowKeys then
		ChatFrameEditBox:SetAltArrowKeyMode(false)
	end
   
	if top then
		local editbox = _G["ChatFrameEditBox"]
		editbox:ClearAllPoints()
		editbox:SetPoint("BOTTOMLEFT",  "ChatFrame1", "TOPLEFT",  -5, 0)
		editbox:SetPoint("BOTTOMRIGHT", "ChatFrame1", "TOPRIGHT", 5, 0)
	end
end
