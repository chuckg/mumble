mumbleScroll = {}

local _G = getfenv(0)

function mumbleScroll:Enable(lines)
	local function scroll(this, delta)
		if delta > 0 then
			if IsShiftKeyDown() then
				this:ScrollToTop()
			elseif IsControlKeyDown() then
				this:PageUp()
			else
				for i = 1, lines do
					this:ScrollUp()
				end
			end
		elseif delta < 0 then
			if IsShiftKeyDown() then
				this:ScrollToBottom()
			elseif IsControlKeyDown() then
				this:PageDown()
			else
				for i = 1, lines do
					this:ScrollDown()
				end
			end
		end
	end

	for i = 1, 7 do
		local cf = _G["ChatFrame"..i]
		cf:SetScript("OnMouseWheel", scroll)
		cf:EnableMouseWheel(true)
	end
end