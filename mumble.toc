## Interface: 20100
## Title: mumble
## Notes: Base chat modifications.
## Version: 1.2
## Author: chuckg (Blindchuck <Ropetown>)
## X-eMail: c@chuckg.org
## X-Website: http://chuckg.wowinterface.com
## X-Category: Interface Enhancements
## X-Date: $Date: 2007-06-15 16:31:29 -0700 (Fri, 15 Jun 2007) $
## OptionalDeps: 
## Dependencies: 

buttons.lua
channelnames.lua
chatfade.lua
editbox.lua
playernames.lua
scroll.lua
sticky.lua
timestamps.lua
telltarget.lua
copypaste.lua

config.lua