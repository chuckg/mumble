-- ************************************************************************************
-- This code is based on SimpleChatMods (which was largely based on idChat) and 
-- Teknicolor, so thanks to all their authors. mumble simply moves away from
-- any dependence on libraries, has some additions and changes that I personally wanted, 
-- as well as some minor improvements.  Check the TOC for contact information regarding
-- bugs and/or suggestions you might have.
-- ************************************************************************************
-- HOWTO:
-- There is no in-game configuration, it is all done through this file.
-- Comment out or delete any :Enable() line/section to disable a mumble feature in the same
-- way this description is commented out/prefixed or like this example (note the brackets,
-- --[[ text ]]--, which are multi-line comments):
--[[ 
mumbleModule:Enable(
		true/false,
		true/false,
		true/false,
)
]]--
-- ************************************************************************************

-- Channel name modifications, %s represents the placement of the players name, ie: "%s yells" = "[Playername] yells:".  
-- Later in the configuration, you may modify the brackets that enclose a player's name.
MUMBLE_CHANNELNAMES_REGEXPS = {
	{ "%[%d+%. General.-%]", 			"|cff990066[|rGEN|cff990066]|r" },		-- Change how [City - General] is displayed.
	{ "%[%d+%. LocalDefense.-%]", 		"|cff990066[|rLD|cff990066]|r" },		-- Change how [Zone - LocalDefense] is displayed.
	{ "%[%d+%. Trade.-%]", 				"|cff990066[|rT|cff990066]|r" },		-- Change how [City - Trade] is displayed.
	{ "%[%d+%. WorldDefense%]", 		"|cff990066[|rWD|cff990066]|r" },		-- Change how [Zone - WorldDefense] is displayed.
	{ "%[%d+%. GuildRecruitment.-%]",	"|cff990066[|rGR|cff990066]|r" },		-- Change how [Zone - WorldDefense] is displayed.
	{ "%[(%d+)%. %a+%]", 			"[%1]" }, 								-- Show channel's as [#] instead of [#. name]
}
mumbleChannelNames:Enable({
	CHAT_GUILD_GET = "[G] %s:", 										-- Guild message from player.
	CHAT_PARTY_GET = "[P] %s:",											-- Party message from player.
	CHAT_RAID_GET = "[R] %s:";	 										-- Raid message from player.
	CHAT_RAID_WARNING_GET = "|cffff0000[|rRW|cffff0000]|r %s:",			-- Raid warning message.		
	CHAT_RAID_LEADER_GET = "|cffff0000[|rRL|cffff0000]|r %s:",			-- Raid message from leader.
	CHAT_OFFICER_GET = "[O] %s:", 										-- Officer chat from player.
	CHAT_BATTLEGROUND_GET = "|cffff3399[|rBG|cffff3399]|r %s:",			-- Battleground message from player.
	CHAT_BATTLEGROUND_LEADER_GET = "|cffff0000[|rBGL|cffff0000]|r %s:",	-- Battleground message from leader.
	CHAT_SAY_GET = "%s:", 												-- Normal message from player.
	CHAT_WHISPER_GET = "From %s:",										-- Whisper from player.
	CHAT_WHISPER_INFORM_GET = "To %s:";									-- A whisper already sent to player %s
	CHAT_YELL_GET = "%s:",												-- Yell from player.
	CHAT_MONSTER_SAY_GET = "%s:",						 				-- Monster chat from monster.
	CHAT_MONSTER_WHISPER_GET = "%s:",									-- Whisper from NPC\Monster.
	CHAT_MONSTER_YELL_GET = "%s:",					 					-- Monster yell form monster named %s.
})

-- Sticky defines which channels "remember" when they were typed in last so you so that
-- you don't have to type /channel again.
mumbleSticky:Enable({
	["SAY"] = true,
	["WHISPER"] = false,
	["YELL"] = false,
	["PARTY"] = true,
	["GUILD"] = true,
	["OFFICER"] = true,
	["RAID"] = true,
	["RAID_WARNING"] = false,
	["BATTLEGROUND"] = true,
	["CHANNEL"] = true,
	["EMOTE"] = false,
})

-- By enabling this module, you *disable* chat text fading in all frames.
mumbleChatFade:Enable()

-- Color player names by class and change what brackets surround them, ie: <Player> or (Player)
mumblePlayerNames:Enable(
		"$name",				-- Format to display the player name. Valid values are: $name and $level. Examples: "$level $name", "$level$name", "$level:$name", "$name$level", etc. Default is just "$name".
								-- If you want to get really tricky with the way the level is displayed and have some understand of color codes, you can even insert your own color codes.  Examples:
								--		White level: |cffFFFFFF$level|r
								--		Black level: |cff000000$level|r
								-- 		Purple level: |cff9382c9$level|r 
								-- If you notice, the pattern is always the same: |cff######$level|r.  If you want a specific colour, just replace those 6 # signs with an HTML colour code. Google it!
		"[",           			-- Left bracket around player names.
        "]"	            		-- Right bracket around player names.
)

-- Editbox modifications.
mumbleEditBox:Enable(
	true,						-- Turn on arrowkeys to navigate through typed text in the default editbox.
	false						-- Move the editbox to the top of the ChatFrame1.
)

-- Chatframe buttons, up/down/bottom
mumbleButtons:Enable(
	false,						-- Show the up button.
	false,						-- Show the down button.
	false						-- Show the "to bottom" button.
)

-- Copy and Paste.
mumbleCopyPaste:Enable(
	"#",						-- The symbol prepended to each line that you click to get it's text for copy&paste.
	"777777",					-- The color of the symbol.
	nil							-- A list of chatframes you *do not* wish to have copy&paste functionality, formated: "#, #, #". Just change this to "nil" (without quotes) if you wish all channels to have copy&paste.
)

-- Timestamp
mumbleTimeStamps:Enable(
	"%X",						-- The format of the timestamp, read up on the function date() if you're really interested.
	false,						-- Show millisecond precision? true/false
	"777777",					-- The color of the timestamp.
	"%s|r %s",					-- The first %s represents the timestamp and the second, your text.
	"1"							-- A list of chatframes you *do not* wish to have timestamps preprended to, formated: "#, #, #".  Just change this to "nil" (without quotes) if you wish all channels to have timestamps.
								-- Example: If you do not want timestamps in ChatFrames 1 and 3, but all others, you would input: "1, 3"
)

-- Telltarget allows you to send a message to your current target: /tt Your long message.
mumbleTellTarget:Enable()

-- Enables mousescrolling in ChatFrames.
mumbleScroll:Enable(
	1                  			-- The number of lines scrolled when using the mousewheel.
)