mumble 1.2

Author: chuckg (c@chuckg.org)
Realm: Blindchuck <Ropetown>, Executus
Website: http://chuckg.org, http://chuckg.wowinterface.com
Date: 2007-05-01 11:39:12

mumble is a collection of chat modifications. It is not dependent on any libraries
to function and is built in such a way that removing features (or modules) is done
very easily.  However, mumble does not come with an in-game configuration, opting 
for a simple (I hope) config.lua.  Please see the HowTo section for details.

Features:
    - Channel name replacement, for example [Guild] becomes [G] or [City - Trade] becomes [T].
    - Change which channels are "sticky", or remember when they were last typed into.
    - Disable ChatText fading.
    - Editbox movement to the top or bottom of the ChatFrame.
    - Hide or remove the up, down and bottom buttons from the side of ChatFrames.
    - Enables mouseScrolling in the ChatFrames, holding down shift+scrolling will page 
      while holding down control+scrolling will move to the top or bottom of the ChatFrame.
    - Timestamps.
    - colour player names by class, show the player's level, and change the brackets that surround them.
    - TellTarget functionality through the "/tt" command.
    - Copy and Paste any message from any ChatFrame.  This isn't just a scrape of the
      entire window, but rather a line by line copy.

HowTo:
As I said before, there is no in-game configuration.  All configuration is done
through the config.lua file which is basically a list of module enables.  Each
module enable has a line by line breakdown of what each argument does, so it 
should be fairly intuitive.  

Each feature is basically it's own module. To disable any module, all that needs 
to be done is comment it out, or delete it.  Here is an example of how you would 
comment out a large section:

--[[ mumbleSomeModule:Enable(
	true/nil,
	true/nil,
	true/nil,
) ]]--

You'll notice that the --[[ ]]-- enclose the enable and therefore make it "inactive"
or you could simply delete the entire section.  


Leave a message for me in the comments or at WoWInterface bug report if you 
have any issues / ideas / updates / etc. to share with me.

TO INSTALL: Put the moveFrames folder into
	\World of Warcraft\Interface\AddOns\
