mumblePlayerNames = {}

local colors = {
	DRUID   = "ff7c0a",
	HUNTER  = "aad372",
	MAGE    = "68ccef",
	PALADIN = "f48cba",
	PRIEST  = "ffffff",
	ROGUE   = "fff468",
	SHAMAN  = "00dbba",
	WARLOCK = "9382c9",
	WARRIOR = "c69b6d"
}
local names = {}

local _G = getfenv(0)
local string_gsub = gsub
local string_match = strmatch

function mumblePlayerNames:Enable(pformat, leftBracket, rightBracket)
	local function getcolor(class)
		class = strupper(class)
		if not _G["RAID_CLASS_COLORS"] then return colors[class] end
		return format("%02x%02x%02x", _G["RAID_CLASS_COLORS"][class].r*255, _G["RAID_CLASS_COLORS"][class].g*255, _G["RAID_CLASS_COLORS"][class].b*255)
	end
	
	local function register(name, level, class)
		if not class or class == "Unknown" or not name or names[name] then return end
		local color = getcolor(class)
		if color == "a0a0a0" then return end
		
		local p = pformat
		if pformat and pformat ~= nil then
			-- gsub the level, if it exists
			p = string_gsub(p, "$level", level)
			-- gsub the name
			p = string_gsub(p, "$name", "|cff"..color..name.."|r")
		else 
			p = "|cff"..color..name.."|r"
		end

		names[name] = leftBracket..p..rightBracket
	end
	
	local eventfuncs = {
		PLAYER_LOGIN = function() 
			if IsInGuild() then GuildRoster() end 
			if GetNumFriends() > 0 then ShowFriends() end
			-- register the player
			local _, class = UnitClass("player")
			register(UnitName("player"), UnitLevel("player"), class)
		end,
		FRIENDLIST_UPDATE = function() 
			for i=1,GetNumFriends() do 
				local name, level, class = GetFriendInfo(i) 
				register(name, level, class)
			end 
		end,
		GUILD_ROSTER_UPDATE = function() 
			for i=1,GetNumGuildMembers(true) do 
				local name, _, _, level,class = GetGuildRosterInfo(i) 
				register(name, level, class)
			end 
		end,
		RAID_ROSTER_UPDATE = function() 
			for i=1,GetNumRaidMembers() do 
				local name, _, _, _, level, class = GetRaidRosterInfo(i) 
				register(name, level, class)
			end 
		end,
		PARTY_MEMBERS_CHANGED = function() 
			for i=1,GetNumPartyMembers() do 
				local unit = "party" .. i 
				local _, class = UnitClass(unit) 
				register(UnitName(unit), UnitLevel(unit), class) 
			end 
		end,
		PLAYER_TARGET_CHANGED = function() 
			if not UnitIsPlayer("target") or not UnitIsFriend("player", "target") then return end 
			local _, class = UnitClass("target") 
			register(UnitName("target"), UnitLevel("target"), class) 
		end,
		WHO_LIST_UPDATE = function() 
			for i=1,GetNumWhoResults() do 
				local name, _, level, _, class = GetWhoInfo(i) 
				register(name, class) 
			end 
		end,
		UPDATE_MOUSEOVER_UNIT = function() 
			if not UnitIsPlayer("mouseover") or not UnitIsFriend("player", "mouseover") then return end 
			local _, class = UnitClass("mouseover") 
			register(UnitName("mouseover"), UnitLevel("mouseover"), class) 
		end,
		CHAT_MSG_SYSTEM = function(self, message) 
			local name, level, class = string_match(message, "^%|Hplayer:%w+|h%[(%w+)%]|h: Level (%d+) %w+%s?[%w]* (%w+)%s?<?[^>]*>? %- .+$") 
			if name and class then 
				register(name, level, class) 
			end 
		end
	}
	
	-- register our events
	local frame = CreateFrame("frame")
	frame.name = "mumblePlayerNames"
	frame:SetScript("OnEvent", function(self, event, ...) if eventfuncs[event] then eventfuncs[event](self, ...) end end)
	for e in pairs(eventfuncs) do 
		frame:RegisterEvent(e) 
	end

	for i=1,7 do
		local f = _G["ChatFrame"..i]
		local add = f.AddMessage
		f.AddMessage = function(this,text,r,g,b,id)
			local name = arg2
			if event == "CHAT_MSG_SYSTEM" then name = string_match(text, "|h%[(.+)%]|h") end
			if name and names[name] then 
				text = string_gsub(text, "|h%["..name.."%]|h", "|h"..names[name].."|h") 
			elseif name then
				text = string_gsub(text, "|h%["..name.."%]|h", "|h"..leftBracket..name..rightBracket.."|h") 
			end
			add(this,text,r,g,b,id)
		end
	end
end