mumbleChannelNames = {}

local _G = getfenv(0)
local string_gsub = gsub

local numChannels = 0
function mumbleChannelNames:Enable(c)
	local pause = false
	numChannels = #MUMBLE_CHANNELNAMES_REGEXPS
	-- Custom channels
	for i=1,7 do
		local f = _G["ChatFrame"..i]
		local add = f.AddMessage
		f.AddMessage = function(this,text,r,g,b,id)
			if type(text) == "string" and not pause then 
				for e=1,numChannels do
					text = string_gsub(text, _G["MUMBLE_CHANNELNAMES_REGEXPS"][e][1], _G["MUMBLE_CHANNELNAMES_REGEXPS"][e][2])
				end
			end
			add(this,text,r,g,b,id)
		end
	end
   
	-- Global channels
	for k,v in pairs(c) do
		_G[k] = v.." "
	end
	
	-- Hook /chatlist
	local listChannel = SlashCmdList["LIST_CHANNEL"]
	SlashCmdList["LIST_CHANNEL"] = function(msg)
		pause = true
		listChannel(msg)
		pause = false
	end
end